import { configureStore } from "@reduxjs/toolkit";
import productsReducr from "../features/productsSlice";
import productsCartReducer from "../features/productsCartSlice";
export default configureStore({
  reducer: {
    products: productsReducr,
    productsCart: productsCartReducer,
  },
});
