import { Dispatch } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  selectProductsCart,
  setProductsCart,
} from "../../features/productsCartSlice";
import { product } from "../../helpers/hepers";
import "../../styles/button.css";

const Button = (product: product) => {
  let productsInCart: product[] = useSelector(selectProductsCart);
  const dispatch: Dispatch<any> = useDispatch();
  //sendProductToCart es la función que se encargará de actualizar el estado de productos en el carrito de compras.
  //También actualiza el localStorage para que cuando el usuario recargue la página, la información del carro de compras quede guardada.
  const sendProductToCart = (product: product) => {
    try {
      let itemInCarr: product | undefined = productsInCart?.find(
        (ele: product) => ele._id === product._id
      );
      if (!itemInCarr) {
        let dataToSendStateAndLocalStorage: product[] = [
          ...productsInCart,
          { ...product, quantity: 1 },
        ];
        dispatch(setProductsCart(dataToSendStateAndLocalStorage));
        localStorage.setItem(
          "productsInCart",
          JSON.stringify(dataToSendStateAndLocalStorage)
        );
      } else {
        let updateProduct: product[] = productsInCart.map((item: product) =>
          item._id === product._id
            ? { ...item, quantity: item.quantity + 1 }
            : item
        );
        dispatch(setProductsCart(updateProduct));
        localStorage.setItem("productsInCart", JSON.stringify(updateProduct));
      }
    } catch (error) {
      //En este caso por tema de simplicidad se muestra en la consola el error si lo llegase a haber.
      console.log(error);
    }
  };
  return (
    <button
      onClick={() => sendProductToCart(product)}
      className="button"
      disabled={product.countInStock !== 0 ? false : true}
    >
      {product.countInStock !== 0 ? "Add item to cart" : "No in Stock"}
    </button>
  );
};
export default Button;
