import Navbar from "../common/Navbar";
import { useSelector, useDispatch } from "react-redux";
import { Dispatch, useEffect } from "react";
import {
  selectProductsCart,
  setProductsCart,
} from "../../features/productsCartSlice";
import ProductCardInCart from "../common/ProductCardInCart";
import "../../styles/cart-page.css";
import {
  checkDataLocalStorageAndUpdateState,
  product,
} from "../../helpers/hepers";

const CartPage = () => {
  let products: product[] = useSelector(selectProductsCart);
  let dispatch: Dispatch<any> = useDispatch();
  useEffect(() => {
    checkDataLocalStorageAndUpdateState(dispatch, setProductsCart);
  }, []);
  // La función calculateTotalAmountToPay  se encarga de sumar el precio de todos los productos que estén en el carrito de compras y retornar dicha suma
  const calculateTotalAmountToPay = (products: product[]) => {
    let total: number = 0;
    if (products.length !== 0) {
      products.forEach((item: product) => {
        total += item.quantity * item.price;
      });
    }
    return total;
  };

  return (
    <>
      <Navbar />
      <div className="cart-page">
        {products.length !== 0 ? (
          products.map((product: product) => (
            <ProductCardInCart key={product._id} {...product} />
          ))
        ) : (
          <span style={{ textAlign: "center" }}>No products in cart</span>
        )}
        {products.length !== 0 && (
          <div className="cart-page__total-compra">
            <p className="cart-page__text">Summary of purchase</p>
            <div className="cart-page__total">
              <span>Total</span>
              <span>{calculateTotalAmountToPay(products).toFixed(2)} USD</span>
            </div>
            <div className="cart-page__container-button-check-out">
              <p className="cart-page__text-button">Choose more products</p>
              <span className="cart-page__button-checkout">
                Finalize purchase
              </span>
            </div>
          </div>
        )}
      </div>
    </>
  );
};
export default CartPage;
