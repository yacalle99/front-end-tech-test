import { useState, useEffect, Dispatch } from "react";
import "../../styles/product-display-page.css";
import ProductDetailCard from "../common/ProductDetailCard";
import { useDispatch } from "react-redux";
import Navbar from "../common/Navbar";
import { setProductsCart } from "../../features/productsCartSlice";
import {
  checkDataLocalStorageAndUpdateState,
  product,
} from "../../helpers/hepers";
const ProductDisplayPage = () => {
  const [product, setProduct] = useState<product>();
  let dispatch: Dispatch<any> = useDispatch();
  useEffect(() => {
    checkDataLocalStorageAndUpdateState(dispatch, setProductsCart);
  }, []);
  /*Para que el componente no se re-rendrice, utilizamos un useEffect para traer los datos del localStorge y actualizar la variable product.
  Y como el usuario al hacer click sobre la imagen de algún producto disparará una función que envía dicho producto al localStorage y luego redirige al usuario a este componente, lo primero ue debe hacer este componente al ser renderizado es traer dicho producto del localStorage y actualizar la variable local product.
  */
  useEffect(() => {
    const getProductLocalStorage: string | null =
      localStorage.getItem("product");
    if (getProductLocalStorage) {
      const productToDisplayInfo: product = JSON.parse(getProductLocalStorage);
      setProduct(productToDisplayInfo);
    }
  }, []);

  return (
    <>
      <Navbar />
      <div className="product-detail__description">
        <h2 className="product-detail__title-h2">Product page</h2>
        {product && <ProductDetailCard {...product} />}
      </div>
    </>
  );
};
export default ProductDisplayPage;
