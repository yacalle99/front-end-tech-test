import axios from "axios";
import { useState, useEffect, Dispatch } from "react";
import ProductCard from "../common/ProductCard";
import "../../styles/product-list-page.css";
import { setProductsCart } from "../../features/productsCartSlice";
import { useDispatch } from "react-redux";
import Navbar from "../common/Navbar";
import {
  checkDataLocalStorageAndUpdateState,
  product,
} from "../../helpers/hepers";
const ProductListPage = () => {
  const [data, setData] = useState<product[]>();
  const dispatch: Dispatch<any> = useDispatch();
  /* En el siguiente useEffect hacemos 2  tareas principales. Se hace la petición a la API y se actualiza la variable 'data'. */
  /*Con el propósito de mantener la persistencia del estado global de la App,  cada que se renderice el componente se  consultará el localStorage por medio de la función checkDataLocalStorageAndUpdateState (Este guardará todos los productos del cart) y si existe algo, se mandará al estado global de la App*/
  useEffect(() => {
    const getData = async () => {
      let result = await axios.get("http://localhost:5000/api/products");
      setData(result.data);
    };
    getData();
    checkDataLocalStorageAndUpdateState(dispatch, setProductsCart);
  }, []);
  return (
    <>
      <Navbar />
      <div className="container-product">
        {data?.length !== 0 &&
          data?.map((product: product) => {
            return <ProductCard key={product._id} {...product} />;
          })}
      </div>
    </>
  );
};

export default ProductListPage;
