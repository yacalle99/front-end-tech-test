import { ActionCreatorWithPayload } from "@reduxjs/toolkit";
import { Dispatch } from "react";
//Esta es la interface que tiene el modelo de la información que debe contener cada objeto de producto.
export interface product {
  brand: string;
  category: string;
  countInStock: number;
  description: string;
  image: string;
  name: string;
  numReviews: number;
  price: number;
  rating: string;
  _id: string;
  quantity: number;
}
/* La función checkDataLocalStorageAndUpdateState se encarga de consultar el localStorage y actualizar el estado global de productos en el carro de compras. Esto se hace para que el usuario siempre tenga visible la información de los productos que tiene en el carrito*/
export const checkDataLocalStorageAndUpdateState = (
  dispatch: Dispatch<any>,
  setProductsCart: ActionCreatorWithPayload<any, string>
) => {
  try {
    let consultLocalStorage: string | null =
      localStorage.getItem("productsInCart");
    if (consultLocalStorage) {
      let dataSendState: product[] = JSON.parse(consultLocalStorage);
      dispatch(setProductsCart(dataSendState));
    }
  } catch (error) {
    console.log(error);
  }
};
